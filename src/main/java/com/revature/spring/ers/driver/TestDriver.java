package com.revature.spring.ers.driver;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.revature.spring.ers.daos.ReimDao;
import com.revature.spring.ers.models.Reimbursement;

public class TestDriver {

	public static void main(String[] args) {
		ApplicationContext ac = new ClassPathXmlApplicationContext("applicationContext.xml");
		
//		UserDao uDao = (UserDao) ac.getBean("userDaoImpl");
//		
//		uDao.save(new User(3, "zeratoxv", "pass", "Zeratox", "Vonreal", "zvonreal@gmail.com", 0));
//		uDao.save(new User(3, "venilev", "pass", "Venile", "Vonreal", "vvonreal@gmail.com", 1));
//		uDao.save(new User(3, "vilettav", "pass", "Viletta", "Vonreal", "vivonreal@gmail.com", 1));
//		
//		uDao.getAll().forEach(user -> {
//			System.out.println(user);
//		});;
		
//		System.out.println(uDao.findById(1));
		
		ReimDao rDao = (ReimDao) ac.getBean("reimDaoImpl");
		
		rDao.save(new Reimbursement(2, 500, "Car got towed"));
		rDao.save(new Reimbursement(2, 100, "Picked up clothing from the cleaners"));
		rDao.save(new Reimbursement(3, 120000, "Sealed the contract with EITC"));
//		
		
//		rDao.findByAuthor(2).forEach(uReim -> {
//			System.out.println(uReim);
//		});
		
//		rDao.findByStatus(0).forEach(reimByS -> {
//			System.out.println(reimByS);
//		});

//		rDao.findByAuthorAndStatus(2, 0).forEach(oReim -> {
//			System.out.println(oReim);
//		});
		
//		rDao.updateReim(1, 500, "Car got towed while running company errand");
//		
//		rDao.findByAuthor(2).forEach(updated -> {
//			System.out.println(updated);
//		});
		
//		rDao.approveOrDeny(1, 1);
		
		rDao.getAll().forEach(reim -> {
			System.out.println(reim);
		});
	}

}
