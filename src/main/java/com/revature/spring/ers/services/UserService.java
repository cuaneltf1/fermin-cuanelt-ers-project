package com.revature.spring.ers.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.revature.spring.ers.daos.UserDao;
import com.revature.spring.ers.models.User;

@Service
public class UserService {

	@Autowired
	private UserDao userDao;
	
	public User findByUsernameAndPassword(String username, String password) {
		return userDao.findByUsernameAndPassword(username, password);
	}
	
	public void save(User u) {
		userDao.save(u);
	}
	
	public List<User> findById(int id) {
		return userDao.findById(id);
	}
	
	public List<User> getAll() {
		return userDao.getAll();
	}
	
}
