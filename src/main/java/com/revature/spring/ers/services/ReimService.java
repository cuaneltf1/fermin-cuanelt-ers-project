package com.revature.spring.ers.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.revature.spring.ers.daos.ReimDao;
import com.revature.spring.ers.models.Reimbursement;

@Service
public class ReimService {
	
	@Autowired
	private ReimDao reimDao;

	public void save (Reimbursement r) {
		reimDao.save(r);
	}
	
	public List<Reimbursement> getAll() {
		return reimDao.getAll();
	}
	
	public List<Reimbursement> findByAuthor(int author) {
		return reimDao.findByAuthor(author);
	}

	public List<Reimbursement> findByStatus(int status) {
		return reimDao.findByStatus(status);
	}
	
	public List<Reimbursement> findByAuthorAndStatus(int author, int status) {
		return reimDao.findByAuthorAndStatus(author, status);
	}

	public void updateReim(int id, double amount, String reason) {
		reimDao.updateReim(id, amount, reason);
	}
	
	public void approveOrDeny(int id, int status) {
		reimDao.approveOrDeny(id, status);
	}
}
