package com.revature.spring.ers.daos;

import java.util.List;

import com.revature.spring.ers.models.Reimbursement;

public interface ReimDao {
	
	// Make a new reimbursement – Shared(Manager reim creation and approvel to be worked on a later time) [Works]
	public void save(Reimbursement r);
	
	// Get all reimbursements - Manager exclusive [Works]
	public List<Reimbursement> getAll();
	
	// Find all reimbursements from a single user — Shared but implementation will be different [Works]
	public List<Reimbursement> findByAuthor(int author);
	
	// Find all pending/approved reimbursements — Manager exclusive [Works, but needs another test]
	public List<Reimbursement> findByStatus(int status);
	
	// Find all pending/approved reimbursements — User exclusive [Works]
	public List<Reimbursement> findByAuthorAndStatus(int author, int status);
	
	// Update a pending request - User exclusive [Works]
	public void updateReim(int id, double amount, String reason);
	
	// Approve/Deny reimbursement — Manager exclusive [Works]
	public void approveOrDeny(int id, int status);

}
