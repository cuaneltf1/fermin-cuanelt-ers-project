package com.revature.spring.ers.daos;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.revature.spring.ers.models.User;

@Repository
public class UserDaoImpl implements UserDao{
	
	@Autowired
	private SessionFactory sf;

	@Override
	@Transactional
	public User findByUsernameAndPassword(String username, String password) {
		Session s = sf.getCurrentSession();
		CriteriaBuilder cb = s.getCriteriaBuilder();
		CriteriaQuery<User> cq = cb.createQuery(User.class);
		Root<User> root = cq.from(User.class);
		Predicate userN = cb.equal(root.get("username"), username);
		Predicate pass = cb.equal(root.get("password"), password);
		cq.select(root).where(cb.and(userN, pass));
		
		Query<User> query = s.createQuery(cq);
		List<User> userAuth = query.getResultList();
		User out = new User();
		for (User u : userAuth) {
			out.setId(u.getId());
			out.setUsername(u.getUsername());
			out.setPassword("*****");
			out.setAuthority(u.getAuthority());
		}
		return out;
	}

	@Override
	@Transactional
	public void save(User u) {
		Session s = sf.getCurrentSession();
		s.save(u);
	}

	@Override
	@Transactional
	public List<User> findById(int id) {
		Session s = sf.getCurrentSession();
		CriteriaBuilder cb = s.getCriteriaBuilder();
		CriteriaQuery<User> cq = cb.createQuery(User.class);
		Root<User> root = cq.from(User.class);
		Predicate userId = cb.equal(root.get("id"), id);
		cq.select(root).where(userId);
		Query<User> query = s.createQuery(cq);
		List<User> user = query.getResultList();
		return user;
	}

	@Override
	@Transactional
	public List<User> getAll() {
		Session s = sf.getCurrentSession();
		CriteriaBuilder cb = s.getCriteriaBuilder();
		CriteriaQuery<User> cq = cb.createQuery(User.class);
		Root<User> root = cq.from(User.class);
		cq.select(root);
		Query<User> query = s.createQuery(cq);
		List<User> users = query.getResultList();
		System.out.println(users);
		return users;
	}


}
