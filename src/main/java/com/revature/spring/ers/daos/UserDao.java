package com.revature.spring.ers.daos;

import java.util.List;

import com.revature.spring.ers.models.User;

public interface UserDao {
	
	// User authentication
	public User findByUsernameAndPassword(String username, String password);
	
	// Creating new users - Admin privilege [Works]
	public void save(User u);
	
	// Retrieve user info — User exclusive [Works]
	public List<User> findById(int id);
	
	// Retrieve all users — Manager exclusive [Works]
	public List<User> getAll();

}
