package com.revature.spring.ers.daos;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.revature.spring.ers.models.Reimbursement;

@Repository
public class ReimDaoImpl implements ReimDao{

	@Autowired
	private SessionFactory sf;
	
	@Override
	@Transactional
	public void save(Reimbursement r) {
		Session s = sf.getCurrentSession();
		r.setTimeSubmitted(LocalDateTime.now());
		s.save(r);
	}

	@Override
	@Transactional
	public List<Reimbursement> getAll() {
		Session s = sf.getCurrentSession();
		CriteriaBuilder cb = s.getCriteriaBuilder();
		CriteriaQuery<Reimbursement> cq = cb.createQuery(Reimbursement.class);
		Root<Reimbursement> root = cq.from(Reimbursement.class);
		cq.select(root);
		
		Query<Reimbursement> query = s.createQuery(cq);
		List<Reimbursement> reimbursements = query.getResultList();
		return reimbursements;
	}

	@Override
	@Transactional
	public List<Reimbursement> findByAuthor(int author) {
		Session s = sf.getCurrentSession();
		CriteriaBuilder cb = s.getCriteriaBuilder();
		CriteriaQuery<Reimbursement> cq = cb.createQuery(Reimbursement.class);
		Root<Reimbursement> root = cq.from(Reimbursement.class);
		Predicate userId = cb.equal(root.get("author"), author);
		cq.select(root).where(userId);
		
		Query<Reimbursement> query = s.createQuery(cq);
		List<Reimbursement> allReimByUser = query.getResultList();
		return allReimByUser;
	}

	@Override
	@Transactional
	public List<Reimbursement> findByStatus(int status) {
		Session s = sf.getCurrentSession();
		CriteriaBuilder cb = s.getCriteriaBuilder();
		CriteriaQuery<Reimbursement> cq = cb.createQuery(Reimbursement.class);
		Root<Reimbursement> root = cq.from(Reimbursement.class);
		Predicate reimStatus = cb.equal(root.get("status"), status);
		cq.select(root).where(reimStatus);
		
		Query<Reimbursement> query = s.createQuery(cq);
		List<Reimbursement> allReimByStatus = query.getResultList();
		return allReimByStatus;
	}

	@Override
	@Transactional
	public List<Reimbursement> findByAuthorAndStatus(int author, int status) {
		Session s = sf.getCurrentSession();
		CriteriaBuilder cb = s.getCriteriaBuilder();
		CriteriaQuery<Reimbursement> cq = cb.createQuery(Reimbursement.class);
		Root<Reimbursement> root = cq.from(Reimbursement.class);
		Predicate userId = cb.equal(root.get("author"), author);
		Predicate reimStatus = cb.equal(root.get("status"), status);
		cq.select(root).where(cb.and(userId, reimStatus));
		
		Query<Reimbursement> query = s.createQuery(cq);
		List<Reimbursement> allReimByAuthAndStatus = query.getResultList();
		return allReimByAuthAndStatus;
	}

	@Override
	@Transactional
	public void updateReim(int id, double amount, String reason) {
		Session s = sf.getCurrentSession();
		CriteriaBuilder cb = s.getCriteriaBuilder();
		CriteriaUpdate<Reimbursement> update = cb.createCriteriaUpdate(Reimbursement.class);
		Root e = update.from(Reimbursement.class);
		Predicate reimId = cb.equal(e.get("id"), id);
		update.set("amount", amount);
		update.set("reason", reason);
		update.where(reimId);
		s.createQuery(update).executeUpdate();
	}

	@Override
	@Transactional
	public void approveOrDeny(int id, int status) {
		Session s = sf.getCurrentSession();
		CriteriaBuilder cb = s.getCriteriaBuilder();
		CriteriaUpdate<Reimbursement> update = cb.createCriteriaUpdate(Reimbursement.class);
		Root e = update.from(Reimbursement.class);
		Predicate reimId = cb.equal(e.get("id"), id);
		update.set("status", status);
		update.where(reimId);
		s.createQuery(update).executeUpdate();
	}

}
