package com.revature.spring.ers.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.revature.spring.ers.models.Reimbursement;
import com.revature.spring.ers.services.ReimService;

@RestController
public class ReimController {
	
	@Autowired
	private ReimService reimService;
	
	@PostMapping("/reimbursement/save")
	public ResponseEntity<Reimbursement> save(@RequestBody Reimbursement r) {
		Reimbursement newR = new Reimbursement(r.getAuthor(), r.getAmount(), r.getReason());
		reimService.save(newR);
		return ResponseEntity.ok().header("Reimbursement made", "newReim").build();
	}
	
	@GetMapping("/reimbursement/getAll")
	public ResponseEntity<List<Reimbursement>> getAll() {
		List<Reimbursement> reims = reimService.getAll();
		if (reims.size() == 0) {
			return ResponseEntity.noContent().build();
		} else {
			return ResponseEntity.ok(reims);
		}
	}
	
	@GetMapping("/reimbursement/{uid}")
	public ResponseEntity<List<Reimbursement>> findByAuthor(@PathVariable ("uid") int id) {
		List<Reimbursement> uReim = reimService.findByAuthor(id);
		return ResponseEntity.status(HttpStatus.OK).header("Getting reim by author", "getByAuth").body(uReim);
	}
	// Need to resolve how to have multiple one parameter mappings
//	@GetMapping("/reimbursement/{status}")
//	public ResponseEntity<List<Reimbursement>> findByStatus(@PathVariable ("status") int status) {
//		List<Reimbursement> sReim = reimService.findByStatus(status);
//		return ResponseEntity.status(HttpStatus.OK).header("Getting reim by status", "getByStat").body(sReim);
//	}
	
	@GetMapping("/reimbursement/{uid}/{status}")
	public ResponseEntity<List<Reimbursement>> findByAuthorAndStatus(@PathVariable ("uid") int id, @PathVariable ("status") int status) {
		List<Reimbursement> pReim = reimService.findByAuthorAndStatus(id, status);
		return ResponseEntity.status(HttpStatus.OK).header("Getting reim by author and status", "getByAuthAndStat").body(pReim);
	}
	
	@PutMapping("/reimbursement/{rid}")
	public ResponseEntity<?> updateReim(@PathVariable ("rid") int id, @RequestBody Reimbursement r) {
		reimService.updateReim(id, r.getAmount(), r.getReason());
		return ResponseEntity.ok().body("Reimbursement has been updated successfully");
	}
	
	@PutMapping("/reimbursement/{rid}/{status}")
	public ResponseEntity<?> approveOrDeny(@PathVariable ("rid") int id, @PathVariable ("status") int status) {
		reimService.approveOrDeny(id, status);
		return ResponseEntity.ok().body("Reimbursement has been approved/denied successfully");
	}

}
