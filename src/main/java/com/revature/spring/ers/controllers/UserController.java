package com.revature.spring.ers.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.revature.spring.ers.models.User;
import com.revature.spring.ers.services.UserService;

@RestController
public class UserController {
	
	@Autowired
	private UserService userService;
	
	@PostMapping("/login")
	public ResponseEntity<User> findByUsernameAndPassword(@RequestBody User u) {
		User user = userService.findByUsernameAndPassword(u.getUsername(), u.getPassword());
		if (user == null) {
			return ResponseEntity.noContent().build();
		} else {
			return ResponseEntity.ok(user);
		}
	}
	
	@GetMapping("/user/getAll")
	public ResponseEntity<List<User>> getAll() {
		List<User> users = userService.getAll();
		if (users.size() == 0) {
			return ResponseEntity.noContent().build();
		} else {
			return ResponseEntity.ok(users);
		}
	}
	
	@GetMapping("/user/{uid}")
	public ResponseEntity<List<User>> findById(@PathVariable ("uid") int id) {
		List<User> users = userService.findById(id);
		return ResponseEntity.status(HttpStatus.OK).header("Getting all users", "getAll").body(users);
	}
	
}
