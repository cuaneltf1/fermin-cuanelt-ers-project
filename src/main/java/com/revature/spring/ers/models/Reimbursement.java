package com.revature.spring.ers.models;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Entity
@Table(name = "reimbursements")
@Component
public class Reimbursement {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "reim_id_seq_gen")
	@SequenceGenerator(name = "reim_id_seq_gen", sequenceName = "reim_id_seq")
	@Column(name = "reim_id")
	private int id;
	
	@Column(name = "author")
	private int author;
	
	@Column(name = "amount")
	private double amount;
	
	@Column(name = "reason")
	private String reason;
	
	@Column(name = "time_submitted")
	private LocalDateTime timeSubmitted;
	
	@Column(name = "status", columnDefinition = "integer default 0")
	private int status;

	public Reimbursement() {
		super();
	}
	
	public Reimbursement(int author) {
		super();
		this.author = author;
	}
	
	public Reimbursement(int author, double amount, String reason) {
		super();
		this.author = author;
		this.amount = amount;
		this.reason = reason;
	}

	public Reimbursement(int id, int author, double amount, String reason, LocalDateTime timeSubmitted, int status) {
		super();
		this.id = id;
		this.author = author;
		this.amount = amount;
		this.reason = reason;
		this.timeSubmitted = timeSubmitted;
		this.status = status;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getAuthor() {
		return author;
	}

	public void setAuthor(int author) {
		this.author = author;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public LocalDateTime getTimeSubmitted() {
		return timeSubmitted;
	}

	public void setTimeSubmitted(LocalDateTime timeSubmitted) {
		this.timeSubmitted = timeSubmitted;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(amount);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + author;
		result = prime * result + id;
		result = prime * result + ((reason == null) ? 0 : reason.hashCode());
		result = prime * result + status;
		result = prime * result + ((timeSubmitted == null) ? 0 : timeSubmitted.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Reimbursement)) {
			return false;
		}
		Reimbursement other = (Reimbursement) obj;
		if (Double.doubleToLongBits(amount) != Double.doubleToLongBits(other.amount)) {
			return false;
		}
		if (author != other.author) {
			return false;
		}
		if (id != other.id) {
			return false;
		}
		if (reason == null) {
			if (other.reason != null) {
				return false;
			}
		} else if (!reason.equals(other.reason)) {
			return false;
		}
		if (status != other.status) {
			return false;
		}
		if (timeSubmitted == null) {
			if (other.timeSubmitted != null) {
				return false;
			}
		} else if (!timeSubmitted.equals(other.timeSubmitted)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Reimbursement [id=" + id + ", author=" + author + ", amount=" + amount + ", reason=" + reason
				+ ", timeSubmitted=" + timeSubmitted + ", status=" + status + "]";
	}

}
