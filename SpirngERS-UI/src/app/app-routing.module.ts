import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then(mod => mod.LoginModule)
  },
  {
    path: 'manager',
    loadChildren: () => import('./pages/manager-home/manager-home.module').then(mod => mod.ManagerHomeModule)
  },
  {
    path: 'user',
    loadChildren: () => import('./pages/user-home/user-home.module').then(mod => mod.UserHomeModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
