import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManagerHomeComponent } from './components/manager-home.component';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ManagerHomeRoutingModule } from './manager-home-routing.module';
import { NavComponent } from '../mult-use/nav/nav.component';
import { RetrieveUsersComponent } from './components/retrieve-users/retrieve-users.component';



@NgModule({
  declarations: [ManagerHomeComponent, NavComponent, RetrieveUsersComponent],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    ManagerHomeRoutingModule
  ],
  exports: [
    ManagerHomeComponent
  ]
})
export class ManagerHomeModule { }
