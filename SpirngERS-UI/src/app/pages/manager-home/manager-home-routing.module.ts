import { RouterModule, Routes } from '@angular/router';
import { ManagerHomeComponent } from './components/manager-home.component';
import { NgModule } from '@angular/core';
import { RetrieveUsersComponent } from './components/retrieve-users/retrieve-users.component';

const routes: Routes = [
    {
        path: '',
        component: ManagerHomeComponent
    },
    {
        path: 'getUsers',
        component: RetrieveUsersComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ManagerHomeRoutingModule {}
