import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RetrieveUsersComponent } from './retrieve-users.component';

describe('RetrieveUsersComponent', () => {
  let component: RetrieveUsersComponent;
  let fixture: ComponentFixture<RetrieveUsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RetrieveUsersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RetrieveUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
