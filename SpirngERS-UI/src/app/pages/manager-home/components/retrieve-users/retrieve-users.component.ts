import { Component, OnInit } from '@angular/core';
import { ManagerService } from 'src/app/services/manager.service';
import { UserCred } from 'src/app/models/user-credentials.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-retrieve-users',
  templateUrl: './retrieve-users.component.html',
  styleUrls: ['./retrieve-users.component.css']
})
export class RetrieveUsersComponent implements OnInit {

  allUsers: UserCred[] = [];
  userSubscription: Subscription;

  constructor(private managerService: ManagerService) { }

  ngOnInit() {

    this.managerService.getAllUsers();
    this.userSubscription = this.managerService.$currentUsers.subscribe(users => {
      this.allUsers = users;
    });
  }

}
