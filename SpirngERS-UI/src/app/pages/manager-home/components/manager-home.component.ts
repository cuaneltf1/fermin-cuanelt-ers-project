import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { UserAuth } from 'src/app/models/user-authentication.model';
import { AuthService } from 'src/app/services/auth.service';
import { ManagerService } from 'src/app/services/manager.service';
import { OldReim } from 'src/app/models/retrieve-reim.model';

@Component({
  selector: 'app-manager-home',
  templateUrl: './manager-home.component.html',
  styleUrls: ['./manager-home.component.css']
})
export class ManagerHomeComponent implements OnInit {

  updatedReim = {
    rid: undefined,
    status: undefined
  };

  allReims: OldReim[] = [];
  reimSubscription: Subscription;

  currentUserSubscription: Subscription;
  currentUser: UserAuth;

  constructor(private authService: AuthService, private managerService: ManagerService) { }

  ngOnInit() {
    this.currentUserSubscription = this.authService.$currentUser.subscribe(user => {
      this.currentUser = user;
    });

    this.managerService.getAllReim();
    this.reimSubscription = this.managerService.$currentReims.subscribe(reims => {
      this.allReims = reims;
    });
  }

  reimStatusChange() {
    this.managerService.reimStatusChange(this.updatedReim.rid, this.updatedReim.status);
  }

}
