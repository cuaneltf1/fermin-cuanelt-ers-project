import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserHomeComponent } from './components/user-home.component';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { UserHomeRoutingModule } from './user-home-routing.module';
import { UnavComponent } from '../multi-use/unav/unav.component';

@NgModule({
  declarations: [UserHomeComponent, UnavComponent],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    UserHomeRoutingModule
  ],
  exports: [
    UserHomeComponent
  ]
})
export class UserHomeModule { }
