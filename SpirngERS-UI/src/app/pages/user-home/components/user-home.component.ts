import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { UserAuth } from 'src/app/models/user-authentication.model';
import { AuthService } from 'src/app/services/auth.service';
import { UserReim } from 'src/app/models/user-reim.model';
import { UserService } from 'src/app/services/user-service.service';

@Component({
  selector: 'app-user-home',
  templateUrl: './user-home.component.html',
  styleUrls: ['./user-home.component.css']
})
export class UserHomeComponent implements OnInit {

  newReim = {
    author: 0,
    amount: 0.0,
    reason: '',
  };

  userReims: UserReim[] = [];
  userReimSubscription: Subscription;

  currentUserSubscription: Subscription;
  currentUser: UserAuth;

  constructor(private authService: AuthService, private userService: UserService) { }

  ngOnInit() {
    this.currentUserSubscription = this.authService.$currentUser.subscribe(user => {
      this.currentUser = user;
    });

    this.userService.getAllUserReim(this.currentUser.id);
    this.userReimSubscription = this.userService.$currentUserReims.subscribe(ureims => {
      this.userReims = ureims;
    });
  }

  createNewReim() {
    this.newReim.author = this.currentUser.id;
    this.userService.createNewReim(this.newReim);
  }

}
