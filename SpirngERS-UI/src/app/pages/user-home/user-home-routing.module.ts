import { RouterModule, Routes } from '@angular/router';
import { UserHomeComponent } from './components/user-home.component';
import { NgModule } from '@angular/core';

const routes: Routes = [
    {
        path: '',
        component: UserHomeComponent
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class UserHomeRoutingModule {}
