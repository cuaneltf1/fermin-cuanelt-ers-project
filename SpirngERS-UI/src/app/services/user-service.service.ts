import { Injectable } from '@angular/core';
import { UserReim } from '../models/user-reim.model';
import { HttpClient } from '@angular/common/http';
import { ReplaySubject } from 'rxjs';
import { UserCred } from '../models/user-credentials.model';
import { NewReim } from '../models/create-reim.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private currentUserReimsStream = new ReplaySubject<UserReim[]>();
  $currentUserReims = this.currentUserReimsStream.asObservable();

  private currentUsersStream = new ReplaySubject<UserCred[]>();
  $currentUsers = this.currentUsersStream.asObservable();

  constructor(private httpClient: HttpClient) { }

  getAllUserReim(uid) {
    this.httpClient.get<UserReim[]>(`http://localhost:8080/SpringERS/reimbursement/${uid}`, {
      withCredentials: true
    }).subscribe(data => {
      this.currentUserReimsStream.next(data);
    },
    err => {
      console.log('Could not retrieve user data');
    }
    );
  }

  createNewReim(newReim) {
    this.httpClient.post<NewReim>(`http://localhost:8080/SpringERS/reimbursement/save`, newReim, {
      withCredentials: true
    }).subscribe(data => {
      this.getAllUserReim(newReim.author);
    },
    err => {
      console.log('Its not supposed to do that');
    });
  }
}
