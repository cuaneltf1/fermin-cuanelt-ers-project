import { Injectable } from '@angular/core';
import { ReplaySubject } from 'rxjs';
import { OldReim } from '../models/retrieve-reim.model';
import { HttpClient } from '@angular/common/http';
import { UserCred } from '../models/user-credentials.model';

@Injectable({
  providedIn: 'root'
})
export class ManagerService {

  private currentReimsStream = new ReplaySubject<OldReim[]>();
  $currentReims = this.currentReimsStream.asObservable();

  private currentUsersStream = new ReplaySubject<UserCred[]>();
  $currentUsers = this.currentUsersStream.asObservable();

  constructor(private httpClient: HttpClient) { }

  getAllReim() {
    this.httpClient.get<OldReim[]>(`http://localhost:8080/SpringERS/reimbursement/getAll`, {
      withCredentials: true
    }).subscribe(data => {
      this.currentReimsStream.next(data);
    },
    err => {
      console.log('no data to display');
    }
    );
  }

  getAllUsers() {
    this.httpClient.get<UserCred[]>(`http://localhost:8080/SpringERS/user/getAll`, {
      withCredentials: true
    }).subscribe(data => {
      this.currentUsersStream.next(data);
    },
    err => {
      console.log('no data to display');
    }
    );
  }

  reimStatusChange(rid, status) {
    this.httpClient.put<any>(`http://localhost:8080/SpringERS/reimbursement/${rid}/${status}`, {
      withCredentials: true
    }).subscribe(data => {
      // Need to figure out a way to take no response as a response of the change occuring
      console.log(data.text());
      this.getAllReim();
    },
    err => {
      console.log('It works but not as it should');
      this.getAllReim();
    }
    );
  }
}
