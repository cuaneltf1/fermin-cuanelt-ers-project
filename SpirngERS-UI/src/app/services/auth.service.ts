import { Injectable } from '@angular/core';
import { UserAuth } from '../models/user-authentication.model';
import { ReplaySubject, Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user = new UserAuth(0, null, null, 0);

  private currentUserStream = new ReplaySubject<UserAuth>(1);
  $currentUser = this.currentUserStream.asObservable();

  private targetUserStream = new ReplaySubject<UserAuth>(1);
  $targetUser = this.targetUserStream.asObservable();

  private loginErrorStream = new Subject<string>();
  $loginError = this.loginErrorStream.asObservable();

  constructor(private httpClient: HttpClient, private router: Router) {
    this.router.navigateByUrl('/login');
  }

  setTargetUser(user: UserAuth) {
    this.targetUserStream.next(user);
  }

  login(credentials) {
    this.httpClient.post<UserAuth>('http://localhost:8080/SpringERS/login', credentials, {
      withCredentials: true
    }).subscribe(
      data => {
        if (data.username !== null && data.authority === 0) {
          this.router.navigateByUrl('manager');
          this.user = data;
          this.currentUserStream.next(data);
        } else if (data.username !== null && data.authority === 1) {
          this.router.navigateByUrl('user');
          this.user = data;
          this.currentUserStream.next(data);
        } else {
          this.loginErrorStream.next('Failed to login');
        }
      }
    );
  }

  logout() {
    this.currentUserStream.next(null);
    this.router.navigateByUrl('/login');
    this.user = new UserAuth(0, null, null, 0);
  }

}
