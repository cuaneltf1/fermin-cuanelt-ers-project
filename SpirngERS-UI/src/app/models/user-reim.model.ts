export class UserReim {

    constructor(

        public id = 0,
        public amount = 0.0,
        public reason = '',
        public status = 0
    ) {}
}