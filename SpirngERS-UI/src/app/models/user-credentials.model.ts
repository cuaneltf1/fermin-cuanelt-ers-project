export class UserCred {

    constructor(

        public id = 0,
        public username = '',
        // tslint:disable-next-line: variable-name
        public first_name = '',
        // tslint:disable-next-line: variable-name
        public last_name = '',
        // tslint:disable-next-line: variable-name
        public user_email = '',
        public authority = 0

    ) {}

}
