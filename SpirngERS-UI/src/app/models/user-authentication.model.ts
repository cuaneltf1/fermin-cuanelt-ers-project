export class UserAuth {

    constructor(

        public id = 0,
        public username = '',
        public password = '',
        public authority = 0

    ) {}

}
